import os
import requests
import json

def load_env_variables():
    try:
        with open('.env') as f:
            for line in f:
                if line.strip() and not line.startswith('#'):
                    key, value = line.strip().split('=', 1)
                    os.environ[key] = value
    except FileNotFoundError:
        print("Error: .env file not found.")
        exit(1)

def get_crumb(jenkins_url, jenkins_username, jenkins_password):
    crumb_url = f"{jenkins_url}/crumbIssuer/api/json"
    response = requests.get(crumb_url, auth=(jenkins_username, jenkins_password))
    if response.status_code == 200:
        crumb_data = json.loads(response.text)
        return crumb_data
    else:
        print("Failed to get crumb:", response.text)
        return None
        print('Response:', create_job_response.text)

def create_job():
    jenkins_url = os.getenv('JENKINS_URL')
    jenkins_username = os.getenv('JENKINS_USERNAME')
    jenkins_password = os.getenv('JENKINS_PASSWORD')
    jenkins_job_name = os.getenv('JENKINS_JOB_NAME')

    if not all([jenkins_url, jenkins_username, jenkins_password]):
        print("Error: Missing required environment variables.")
        exit(1)

    session = requests.Session()
    session.auth = (jenkins_username, jenkins_password)

    # Get the crumb
    crumb_url = f"{jenkins_url}/crumbIssuer/api/json"
    crumb_response = session.get(crumb_url)
    if crumb_response.status_code == 200:
        crumb_data = crumb_response.json()
        crumb_field = crumb_data['crumbRequestField']
        crumb_value = crumb_data['crumb']
        session.headers.update({crumb_field: crumb_value})
    else:
        print("Failed to get crumb:", crumb_response.text)
        exit(1)

    job_config = '''
    <project>
      <builders>
        <hudson.tasks.Shell>
          <command>echo "Hello, Jenkins!"</command>
        </hudson.tasks.Shell>
      </builders>
    </project>
    '''

    job_name = jenkins_job_name
    create_job_url = f'{jenkins_url}/createItem?name={job_name}'
    headers = {'Content-Type': 'application/xml'}
    create_job_response = session.post(create_job_url, data=job_config, headers=headers)

    if create_job_response.status_code == 200:
        print('Job created successfully')
    else:
        print('Failed to create job')
        print('Response:', create_job_response.text)

if __name__ == '__main__':
    load_env_variables()
    create_job()
