import os
import sys
from utils import load_env_variables, get_jenkins_session

def create_job(job_name, job_config_file):
    jenkins_url = os.getenv('JENKINS_URL')

    with open(job_config_file, 'r') as file:
        job_config = file.read()

    session = get_jenkins_session()

    create_job_url = f'{jenkins_url}/createItem?name={job_name}'
    headers = {'Content-Type': 'application/xml'}
    create_job_response = session.post(create_job_url, data=job_config, headers=headers)

    if create_job_response.status_code == 200:
        print(f'Job "{job_name}" created successfully')
    else:
        print(f'Failed to create job "{job_name}"')
        print('Response:', create_job_response.text)

if __name__ == '__main__':
    load_env_variables()
    
    # Check if the correct number of arguments is provided
    if len(sys.argv) != 3:
        print('Usage: python create_job.py <job_name> <job_config_file>')
        sys.exit(1)
    
    job_name = sys.argv[1]
    job_config_file = sys.argv[2]
    
    create_job(job_name, job_config_file)

