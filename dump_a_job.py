import os
import sys
from urllib.parse import urlparse, unquote
from utils import load_env_variables, get_jenkins_session

def ensure_directory(path):
    """Ensure that a directory exists; if not, create it."""
    if not os.path.exists(path):
        os.makedirs(path)

def get_sanitized_path(segment):
    """Sanitize path segments to create valid directory names."""
    return segment.replace('/', '_').replace(' ', '_')

def construct_job_config_url(jenkins_url, job_path):
    """Correctly constructs the URL to fetch a job's configuration XML."""
    # Ensure there's no leading or trailing slash
    job_path = job_path.strip("/")
    # Correctly format the job path into the Jenkins URL
    formatted_path = '/job/'.join(job_path.split('/'))
    return f"{jenkins_url}/job/{formatted_path}/config.xml"
def dump_job_config(jenkins_url, job_path, job_name, output_dir):
    session = get_jenkins_session()

    config_url = construct_job_config_url(jenkins_url, job_path)
    print(f"Attempting to retrieve config from: {config_url}")  # Debugging output

    config_response = session.get(config_url)
    if config_response.status_code == 200:
        job_config = config_response.text
        # Create a directory for the job
        job_dir = os.path.join(output_dir, get_sanitized_path(job_name))
        ensure_directory(job_dir)
        # Save the configuration file as config.xml within the job's directory
        output_file = os.path.join(job_dir, "config.xml")
        with open(output_file, 'w') as file:
            file.write(job_config)
        print(f'Job configuration dumped to {output_file}')
    else:
        print(f'Failed to retrieve configuration for job at "{config_url}"')
        print('Response:', config_response.text)
def dump_configs_from_url(jenkins_url, job_path, session, output_dir, recursive=True):
    full_url = construct_job_config_url(jenkins_url, job_path).replace('/config.xml', '/api/json?tree=jobs[name,url]')
    jobs_response = session.get(full_url)

    if jobs_response.status_code == 200:
        jobs_data = jobs_response.json()
        for job in jobs_data.get('jobs', []):
            sub_job_path = '/'.join([job_path, job['name']]).strip('/')
            if 'Folder' in job['_class'] and recursive:
                print(f"Entering folder: {job['name']}")
                dump_configs_from_url(jenkins_url, sub_job_path, session, output_dir, recursive)
            else:
                dump_job_config(jenkins_url, sub_job_path, job['name'], output_dir)
    else:
        print('Failed to retrieve job list')
        print('Response:', jobs_response.text)

if __name__ == '__main__':
    load_env_variables()
    jenkins_url = os.getenv('JENKINS_URL').rstrip('/')

    if len(sys.argv) < 3:
        print('Usage: python dump_a_job.py <job_path> <output_directory> [--recursive]')
        sys.exit(1)

    job_path = sys.argv[1]
    output_dir = sys.argv[2]
    recursive = '--recursive' in sys.argv  # Check if the recursive flag is set

    ensure_directory(output_dir)
    dump_configs_from_url(jenkins_url, job_path, get_jenkins_session(), output_dir, recursive)

