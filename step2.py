import os
import requests
import json

def load_env_variables():
    try:
        with open('.env') as f:
            for line in f:
                if line.strip() and not line.startswith('#'):
                    key, value = line.strip().split('=', 1)
                    os.environ[key] = value
    except FileNotFoundError:
        print("Error: .env file not found.")
        exit(1)

def verify_crumb_retrieval():
    jenkins_url = os.getenv('JENKINS_URL')
    jenkins_username = os.getenv('JENKINS_USERNAME')
    jenkins_password = os.getenv('JENKINS_PASSWORD')

    if not all([jenkins_url, jenkins_username, jenkins_password]):
        print("Error: Missing required environment variables.")
        exit(1)

    crumb_url = f"{jenkins_url}/crumbIssuer/api/json"
    response = requests.get(crumb_url, auth=(jenkins_username, jenkins_password))
    if response.status_code == 200:
        crumb_data = json.loads(response.text)
        print("Crumb retrieved successfully:")
        print(crumb_data)
    else:
        print("Failed to retrieve crumb")
        print("Response:", response.text)

if __name__ == '__main__':
    load_env_variables()
    verify_crumb_retrieval()
