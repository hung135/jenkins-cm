import os
import requests
import json

def load_env_variables():
    try:
        with open('.env') as f:
            for line in f:
                if line.strip() and not line.startswith('#'):
                    key, value = line.strip().split('=', 1)
                    os.environ[key] = value
    except FileNotFoundError:
        print("Error: .env file not found.")
        exit(1)

def get_crumb(jenkins_url, jenkins_username, jenkins_password):
    crumb_url = f"{jenkins_url}/crumbIssuer/api/json"
    response = requests.get(crumb_url, auth=(jenkins_username, jenkins_password))
    if response.status_code == 200:
        crumb_data = json.loads(response.text)
        return crumb_data
    else:
        print("Failed to get crumb:", response.text)
        return None

def create_pipeline():
    jenkins_url = os.getenv('JENKINS_URL')
    jenkins_username = os.getenv('JENKINS_USERNAME')
    jenkins_password = os.getenv('JENKINS_PASSWORD')
    git_repo_url = os.getenv('GIT_REPO_URL')
    jenkins_hook_file = os.getenv('JENKINS_HOOK_FILE')

    if not all([jenkins_url, jenkins_username, jenkins_password, git_repo_url, jenkins_hook_file]):
        print("Error: Missing required environment variables.")
        exit(1)

    crumb_data = get_crumb(jenkins_url, jenkins_username, jenkins_password)
    if not crumb_data:
        exit(1)

    headers = {
        'Content-Type': 'application/xml',
        crumb_data['crumbRequestField']: crumb_data['crumb']
    }

    pipeline_definition = f'''
    <flow-definition plugin="workflow-job@2.40">
      <definition class="org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition" plugin="workflow-cps@2.86">
        <scm class="hudson.plugins.git.GitSCM" plugin="git@4.5.0">
          <configVersion>2</configVersion>
          <userRemoteConfigs>
            <hudson.plugins.git.UserRemoteConfig>
              <url>{git_repo_url}</url>
            </hudson.plugins.git.UserRemoteConfig>
          </userRemoteConfigs>
          <branches>
            <hudson.plugins.git.BranchSpec>
              <name>*/master</name>
            </hudson.plugins.git.BranchSpec>
          </branches>
          <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
          <submoduleCfg class="list"/>
          <extensions/>
        </scm>
        <scriptPath>{jenkins_hook_file}</scriptPath>
        <lightweight>true</lightweight>
      </definition>
    </flow-definition>
    '''

    job_name = "test_jenkins_job"
    response = requests.post(
        f'{jenkins_url}/createItem?name={job_name}',
        auth=(jenkins_username, jenkins_password),
        headers=headers,
        data=pipeline_definition
    )

    if response.status_code == 200 and 'Created' in response.text:
        print('Pipeline created successfully')
    else:
        print('Failed to create pipeline')
        print('Response:', response.text)

if __name__ == '__main__':
    load_env_variables()
    create_pipeline()
