# Import necessary functions from utils module
import os
from utils import load_env_variables, get_jenkins_session

def list_installed_plugins():
    # Load environment variables
    load_env_variables()

    # Get Jenkins session
    session = get_jenkins_session()

    # Jenkins URL from environment variables
    jenkins_url = os.getenv('JENKINS_URL')
    if not jenkins_url:
        print("Error: JENKINS_URL environment variable is missing.")
        exit(1)

    # Get the list of installed plugins
    plugins_url = f"{jenkins_url}/pluginManager/api/json?depth=1"
    plugins_response = session.get(plugins_url)

    if plugins_response.status_code == 200:
        plugins_data = plugins_response.json()
        plugins = plugins_data['plugins']

        if len(plugins) > 0:
            print("Installed Jenkins plugins:")
            for plugin in plugins:
                print(f"{plugin['shortName']} (version: {plugin['version']})")
        else:
            print("No plugins found in Jenkins.")
    else:
        print("Failed to retrieve plugins")
        print("Response:", plugins_response.text)

if __name__ == '__main__':
    list_installed_plugins()

