import argparse
import os 
import sys
from utils import load_env_variables, get_jenkins_session

def list_plugins(session, jenkins_url):
    """List all available plugins from the Jenkins update center."""
    update_center_url = f"{jenkins_url}/updateCenter/api/json?pretty=true&depth=2"
    response = session.get(update_center_url, verify=False)
    if response.status_code == 200:
        try:
            # Adjusting JSON parsing to correctly access the 'availables' key
            available_plugins = response.json().get('availables', [])
            if available_plugins:  # If available_plugins is not empty
                print("Available Plugins:")
                for plugin in available_plugins:
                    print(f"{plugin.get('name')} - {plugin.get('title')}, Version: {plugin.get('version')}")
            else:
                print("No available plugins data found in the response.")
        except ValueError:
            print("Error parsing the JSON response.")
    else:
        print("Failed to retrieve plugins")
        print("Response:", response.text)

def install_plugin(session, jenkins_url, plugin_name):
    """Install a specified plugin."""
    # Construct the API endpoint URL for plugin installation
    install_url = f"{jenkins_url}/pluginManager/installNecessaryPlugins"
    headers = {'Content-Type': 'application/xml'}
    # XML payload to specify the plugin to install
    data = f'<jenkins><install plugin="{plugin_name}@latest" /></jenkins>'
    response = session.post(install_url, data=data, headers=headers, verify=False)
    if response.status_code in [200, 202]:
        print(f"Plugin installation triggered for: {plugin_name}")
    else:
        print("Failed to install plugin")
        print("Response:", response.text)

def main():
    load_env_variables()
    session = get_jenkins_session()
    jenkins_url = os.getenv('JENKINS_URL')

    parser = argparse.ArgumentParser(description='Manage Jenkins plugins.')
    parser.add_argument('--list', help='List all available plugins', action='store_true')
    parser.add_argument('--install', help='Install a specific plugin', type=str)
    
    args = parser.parse_args()
    if args.list:
        list_plugins(session, jenkins_url)
    elif args.install:
        install_plugin(session, jenkins_url, args.install)
    else:
        parser.print_help()

if __name__ == "__main__":
    main()

