import os
import sys
from urllib.parse import urlparse, unquote
from utils import load_env_variables, get_jenkins_session

def ensure_directory(path):
    """Ensure that a directory exists; if not, create it."""
    if not os.path.exists(path):
        os.makedirs(path)

def dump_job_config(job_url, output_dir):
    jenkins_url = os.getenv('JENKINS_URL').rstrip('/')
    session = get_jenkins_session()

    # Parse the job URL to create directories
    parsed_url = urlparse(job_url)
    path = unquote(parsed_url.path)  # Decode URL-encoded characters
    segments = path.strip('/').split('/')
    
    # Construct directory path and ensure it exists
    dir_path = os.path.join(output_dir, *segments[:-1])  # Exclude the job name itself for the directory path
    ensure_directory(dir_path)
    
    # Construct the full URL to get the job configuration
    config_url = f"{jenkins_url}/{job_url}/config.xml"
    print(f"Attempting to retrieve config from: {config_url}")

    config_response = session.get(config_url)
    if config_response.status_code == 200:
        job_config = config_response.text
        output_file = os.path.join(dir_path, f"{segments[-1]}_config.xml")
        with open(output_file, 'w') as file:
            file.write(job_config)
        print(f'Job configuration dumped to {output_file}')
    else:
        print(f'Failed to retrieve configuration for job at "{config_url}"')
        print('Response:', config_response.text)

# Modify dump_configs_from_url and dump_all_job_configs as needed, especially focusing on handling output_dir correctly

if __name__ == '__main__':
    load_env_variables()

    if '--all' in sys.argv:
        output_dir = "jenkins_job_configs"  # Specify your output directory here
        ensure_directory(output_dir)
        dump_all_job_configs()
    else:
        if len(sys.argv) != 3:
            print('Usage: python dump_job.py [--all | <job_url_path> <output_directory>]')
            sys.exit(1)

        job_url_path = sys.argv[1]  # Adjust based on actual usage, considering full path after domain
        output_dir = sys.argv[2]
        ensure_directory(output_dir)  # Make sure the output directory exists
        dump_job_config(job_url_path, output_dir)

