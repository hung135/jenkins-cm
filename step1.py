import os
import requests

def load_env_variables():
    try:
        with open('.env') as f:
            for line in f:
                if line.strip() and not line.startswith('#'):
                    key, value = line.strip().split('=', 1)
                    os.environ[key] = value
    except FileNotFoundError:
        print("Error: .env file not found.")
        exit(1)

def verify_authentication():
    jenkins_url = os.getenv('JENKINS_URL')
    jenkins_username = os.getenv('JENKINS_USERNAME')
    jenkins_password = os.getenv('JENKINS_PASSWORD')

    if not all([jenkins_url, jenkins_username, jenkins_password]):
        print("Error: Missing required environment variables.")
        exit(1)

    response = requests.get(jenkins_url, auth=(jenkins_username, jenkins_password))
    if response.status_code == 200:
        print("Authentication successful")
    else:
        print("Authentication failed")
        print("Response:", response.text)

if __name__ == '__main__':
    load_env_variables()
    verify_authentication()
