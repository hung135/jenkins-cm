import os
import requests

def load_env_variables():
    try:
        with open('.env') as f:
            for line in f:
                if line.strip() and not line.startswith('#'):
                    key, value = line.strip().split('=', 1)
                    os.environ[key] = value
    except FileNotFoundError:
        print("Error: .env file not found.")
        exit(1)

def list_jobs():
    jenkins_url = os.getenv('JENKINS_URL')
    jenkins_username = os.getenv('JENKINS_USERNAME')
    jenkins_password = os.getenv('JENKINS_PASSWORD')

    if not all([jenkins_url, jenkins_username, jenkins_password]):
        print("Error: Missing required environment variables.")
        exit(1)

    session = requests.Session()
    session.auth = (jenkins_username, jenkins_password)

    # Get the list of jobs
    jobs_url = f"{jenkins_url}/api/json?tree=jobs[name]"
    jobs_response = session.get(jobs_url)

    if jobs_response.status_code == 200:
        jobs_data = jobs_response.json()
        jobs = jobs_data['jobs']

        if len(jobs) > 0:
            print("Jobs found in Jenkins:")
            for job in jobs:
                print(job['name'])
        else:
            print("No jobs found in Jenkins.")
    else:
        print("Failed to retrieve jobs")
        print("Response:", jobs_response.text)

if __name__ == '__main__':
    load_env_variables()
    list_jobs()
