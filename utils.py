import os
import requests

def load_env_variables():
    try:
        with open('.env') as f:
            for line in f:
                if line.strip() and not line.startswith('#'):
                    key, value = line.strip().split('=', 1)
                    os.environ[key] = value
    except FileNotFoundError:
        print("Error: .env file not found.")
        exit(1)

def get_jenkins_session():
    jenkins_url = os.getenv('JENKINS_URL')
    jenkins_username = os.getenv('JENKINS_USERNAME')
    jenkins_password = os.getenv('JENKINS_PASSWORD')

    if not all([jenkins_url, jenkins_username, jenkins_password]):
        print("Error: Missing required environment variables.")
        exit(1)

    session = requests.Session()
    session.auth = (jenkins_username, jenkins_password)

    # Get the crumb
    crumb_url = f"{jenkins_url}/crumbIssuer/api/json"
    crumb_response = session.get(crumb_url)
    if crumb_response.status_code == 200:
        crumb_data = crumb_response.json()
        crumb_field = crumb_data['crumbRequestField']
        crumb_value = crumb_data['crumb']
        session.headers.update({crumb_field: crumb_value})
    else:
        print("Failed to get crumb:", crumb_response.text)
        exit(1)

    return session
