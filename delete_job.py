import os
import sys
import requests
from utils import load_env_variables, get_jenkins_session

# Load environment variables from .env file
load_env_variables()

# Define the delete_job function
def delete_job(job_name):
    jenkins_url = os.getenv('JENKINS_URL')

    if not jenkins_url:
        print("Error: Missing required environment variables.")
        sys.exit(1)

    session = get_jenkins_session()

    # Get the crumb
    crumb_url = f"{jenkins_url}/crumbIssuer/api/json"
    crumb_response = session.get(crumb_url)
    if crumb_response.status_code == 200:
        crumb_data = crumb_response.json()
        crumb_field = crumb_data['crumbRequestField']
        crumb_value = crumb_data['crumb']
        session.headers.update({crumb_field: crumb_value})
    else:
        print("Failed to get crumb:", crumb_response.text)
        sys.exit(1)

    # Delete the job
    delete_job_url = f'{jenkins_url}/job/{job_name}/doDelete'
    delete_job_response = session.post(delete_job_url)

    if delete_job_response.status_code == 200:
        print(f'Job "{job_name}" deleted successfully')
    else:
        print(f'Failed to delete job "{job_name}"')
        #print('Response:', delete_job_response.text)
        # Extract and print the title from HTML response
        title_start = delete_job_response.text.find('<title>')
        title_end = delete_job_response.text.find('</title>')
        if title_start != -1 and title_end != -1:
            title = delete_job_response.text[title_start + len('<title>'):title_end]
            print('Error Title:', title)

# Check if the script is being run as the main program
if __name__ == '__main__':
    # Check if the correct number of arguments is provided
    if len(sys.argv) != 2:
        print('Usage: python delete_job.py <job_name>')
        sys.exit(1)
    
    # Extract job name from command-line argument
    job_name = sys.argv[1]
    
    # Call the delete_job function with the provided job name
    delete_job(job_name)

